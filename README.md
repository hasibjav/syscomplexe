# Model Checker CTL

L'objectif de ce projet est de réaliser un model checker pour la logique CTL.


# Installation

- Pour faire marcher ce projet vous aurez besoin d’un navigateur de votre choix ainsi qu’une connexion Internet.

- Téléchargez ou clonez notre projet Model Checker CTL sur notre gitlab : https://gitlab.com/hasibjav/syscomplexe


- Ensuite, ouvrez le fichier modelCheckerCTL.html dans un navigateur, vous aurez alors accès à la page principale du projet.


- Enfin, il vous suffira de choisir un fichier JSON à tester ( ils se trouvent dans le dossier “source”  du projet).
