$( document ).ready(function() {
    console.log( "ready!" );

    // selectorInput contient le input avec l'id selectorInput
    var selectorInput = $('#selectorInput');

    // lorsque l'utilisateur click sur la balise a, on fais un click sur selectorInput
    $('#selectorLink').on('click', function(e) {
      if (selectorInput) {
        selectorInput.click();
      }
      e.preventDefault(); // empêche la navigation vers "#"
    });

});


//==============================================================================
//================ Fonctions pour la structure et la formule ===================
//==============================================================================


//------------------------------------------------------------------------------
//-------------- Fonction qui gère la lecture des fichiers JSON ----------------
//------------------------------------------------------------------------------
function readFile(input) {
  var file = input.files[0];
  //on renvoie un message d'erreur si l'utilisateur n'insère pas un fichier JSON
  if(file.type=="application/json"){
    $('#errorJSONType').removeClass('d-block');
    $('#errorJSONType').addClass('d-none');
    var reader = new FileReader();
    reader.readAsText(file);

    reader.onload = function() {
      var dataTest=JSON.parse(reader.result);
      var ks = dataTest.KS;
      $("#CTLToCheck").html("La formule CTL à vérifier est la suivante : "+dataTest.CTL+".");
      ctlType(ks,dataTest.CTL);
    };

    reader.onerror = function() {
      console.log(reader.error);
    };
  }
  else{
    $('#errorJSONType').removeClass('d-none');
    $('#errorJSONType').addClass('d-block');
  }
}
//..............................................................................


//------------------------------------------------------------------------------
//--------- Fonction qui identifie le type de formule CTL rencontrée -----------
//------------------------------------------------------------------------------
function ctlType(ks,ctl){
  //boolean qui vérifie si la formule CTL à été identifiée
  var checkIdentity = false;
  //on va construire nos regex avec le tableau des propositions atomiques de ks
  var pa = findPA(ks);
  for (var i = 0; i < pa.length; i++){
    //cas req1
    var expReq1 = '^'+pa[i]+'$';
    var regxReq1 = new RegExp(expReq1);
    if(regxReq1.test(ctl)){
      checkIdentity = true;
      //Appel de la fonction casReq1
      var casreq1=casReq1(ks, ctl);
      //Appel de afficheResultat pour afficher le résultat de la vérification
      afficheResultat(casreq1);
    }

    //cas ¬req1
    var expNonReq1 = '^¬'+pa[i]+'$';
    var regxNonReq1 = new RegExp(expNonReq1);
    if(regxNonReq1.test(ctl)){
      checkIdentity = true;
      //Appel de la fonction casNonReq1
      var casnonreq1=casNonReq1(ks,ctl);
      afficheResultat(casnonreq1);
    }

    //cas req1∧Req2
    // on vérifie d'abord req1∧
    var expReq1Ou = '^'+pa[i]+'(?=∧)';
    var regxReq1Ou = new RegExp(expReq1Ou);
    if(regxReq1Ou.test(ctl)){
      for (var j = 0; j < pa.length; j++){
        // on vérifie req1∧req2
        if(pa[i]!==pa[j]){
          var expReq1OuReq2 = pa[i]+'∧'+'(?='+pa[j]+'$)';
          var regxReq1OuReq2 = new RegExp(expReq1OuReq2);
          if(regxReq1OuReq2.test(ctl)){
            checkIdentity = true;
            //Appel de la fonction casReq1EtReq2
            var casreq1etreq2=casReq1EtReq2(ks,pa[i],pa[j]);
            afficheResultat(casreq1etreq2);
          }
        }
      }
    }

    //cas EXreq1
    var expEXReq1 = '^EX(?='+pa[i]+'$)';
    var regxEXReq1 = new RegExp(expEXReq1);
    if(regxEXReq1.test(ctl)){
      checkIdentity = true;
      //Appel de la fonction casNextReq1
      var casXReq1 = casNextReq1(ks, pa[i]);
      afficheResultat(casXReq1);
    }

    //cas Ereq1Ureq2
    //on vérifie d'abord Ereq1U
    var expEU = '^E'+pa[i]+'(?=U)';
    var regxEU = new RegExp(expEU);
    if(regxEU.test(ctl)){
      for (var k = 0; k < pa.length; k++){
        // on vérifie Ereq1Ureq2
        if(pa[i]!==pa[k]){
          var expEUReq2 = '^E'+pa[i]+'U'+pa[k];
          var regxEUReq2 = new RegExp(expEUReq2);
          if(regxEUReq2.test(ctl)){
            checkIdentity = true;
            //Appel de la fonction casEU
            var cas5EU = casEU(ks, pa[i], pa[k]);
            afficheResultat(cas5EU);
          }
        }
      }
    }

    //cas Areq1Ureq2
    //on vérifie d'abord Areq1U
    var expAU = '^A'+pa[i]+'(?=U)';
    var regxAU = new RegExp(expAU);
    if(regxAU.test(ctl)){
      for (var l = 0; l < pa.length; l++){
        // on vérifie Ereq1Ureq2
        if(pa[i]!==pa[l]){
          var expAUReq2 = '^A'+pa[i]+'U'+pa[l];
          var regxAUReq2 = new RegExp(expAUReq2);
          if(regxAUReq2.test(ctl)){
            checkIdentity = true;
            //Appel de la fonction casAU
            var cas6AU = casAU(ks, pa[i], pa[l]);
            afficheResultat(cas6AU);
          }
        }
      }
    }
  }

  //si la formule n'est pas identifiable, on affiche un message d'erreur
  if(checkIdentity == false){
    $('#errorCTL').removeClass('d-none');
    $('#errorCTL').addClass('d-block');
  }
  else{
    $('#errorCTL').removeClass('d-block');
    $('#errorCTL').addClass('d-none');
  }
}
//..............................................................................


//==============================================================================
//======================= Fonctions Principales ================================
//==============================================================================


//------------------------------------------------------------------------------
//------------------- Fonction pour vérifier 0=req1 ----------------------------
//------------------------------------------------------------------------------
function casReq1(ks, ctl){
  //on génère la représentation graphique de ks en appelant la fonction generateGraph
  var myDiagram=generateGraph(ks);
  var statesMarked = [];
  for (var i = 0; i < ks.etatsTransitions.length; i++){
    //Si l'état vérifie la formule CTL, on le met dans statesMarked et on colorie l'état
      if (ks.etatsTransitions[i][1] === ctl){
        statesMarked.push(ks.etatsTransitions[i][2]);
        //un état est découpé en une node avec son bord et une sous node avec son bord, on va les colorier de façon adéquate
        changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"#00FF00");
        changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"#00FF00");
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
      }
      else if(getNode(myDiagram,ks.etatsTransitions[i][2]).color!="#00FF00"){
        changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
      }
  }
  statesMarked = retireDoublons(statesMarked); //On enlève les doublons
  return statesMarked;
}
//..............................................................................


//------------------------------------------------------------------------------
//------------------- Fonction pour vérifier 0=¬req1 ---------------------------
//------------------------------------------------------------------------------
  function casNonReq1(ks,ctl){
    //on génère la représentation graphique de ks en appelant la fonction generateGraph
    var myDiagram=generateGraph(ks);
    //on crée un nouveau tableau statesMarked qui contient les éléments de ks.etats
    var statesMarked = ks.etats.slice();
    //markReq1 contiendra les états qui vérifient req1
    var markReq1=[];
    ctl=ctl.replace('¬','');
    //On parcours les états de transitions et on marque les états qui vérifient req1 comme pour casreq1
    for (var i = 0; i < ks.etatsTransitions.length; i++){
      //Si l'état vérifie la formule CTL, on le met dans statesMarked
        if (ks.etatsTransitions[i][1] === ctl){
          markReq1.push(ks.etatsTransitions[i][2]);
          //coloriage des états
          changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
          changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
          changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
          changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
        }
        //coloriage des états (on évite de colorier un état déjà colorié en vert)
        else if(getNode(myDiagram,ks.etatsTransitions[i][2]).outline!="#00FF00"){
          changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
          changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"#00FF00");
          changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"#00FF00");
          changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
        }
    }
    markReq1 = retireDoublons(markReq1); //On enlève les doublons
    //on enlève enfin les états de statesMarked qui vérifient req1
    for (var j=0; j < markReq1.length; j++){
      statesMarked.splice(statesMarked.indexOf(markReq1[j]),1);
    }
    return statesMarked;
  }
//..............................................................................


//------------------- Fonction pour vérifier 0=req1∧req2 -----------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  function casReq1EtReq2(ks,req1,req2){
    //on génère la représentation graphique de ks en appelant la fonction generateGraph
    var myDiagram=generateGraph(ks);
    var statesMarked=[];
    var markReq1=[];
    var markReq2=[];
    //on marque les états qui vérifient req1 dans markReq1 et ceux qui vérifient req2 dans markReq2
    for (var i=0; i < ks.etatsTransitions.length; i++){
      if (ks.etatsTransitions[i][1] === req1 ){
        markReq1.push(ks.etatsTransitions[i][2]);
        changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
      }
      if (ks.etatsTransitions[i][1] === req2 ){
        markReq2.push(ks.etatsTransitions[i][2]);
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
      }
      else if(getNode(myDiagram,ks.etatsTransitions[i][2]).bord!="#00FF00"){
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
      }
      if(getNode(myDiagram,ks.etatsTransitions[i][2]).outline!="#00FF00"){
        changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
      }
      changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
      changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
    }
    //on enlève les doublons
    markReq1 = retireDoublons(markReq1);
    markReq2 = retireDoublons(markReq2);
    //greaterMark contient le plus grand tableau entre markReq1 et markReq2, l'inverse pour smallerMark
    var greaterMark;
    var smallerMark;
    if(markReq1.kength>markReq2){
      greaterMark=markReq1;
      smallerMark=markReq2;
    }
    else{
      greaterMark=markReq2;
      smallerMark=markReq1;
    }
    //on va ensuite seulement garder les états qui sont à la fois dans markReq1 et markReq2
    for(var j=0; j < smallerMark.length; j++){
      //on garde l'état
      if ( greaterMark.indexOf(smallerMark[j])!=-1){
        statesMarked.push(smallerMark[j]);
        changeNodeColor(myDiagram,smallerMark[j],"#00FF00");
        changeSubNodeColor(myDiagram,smallerMark[j],"#00FF00");
        changeSubNodeBord(myDiagram,smallerMark[j],"#00FF00",2);
        changeNodeBord(myDiagram,smallerMark[j],"#00FF00",2);
      }
    }
    return statesMarked;
  }
//..............................................................................


//------------------------------------------------------------------------------
//------------------- Fonction pour vérifier EXreq1 ----------------------------
//------------------------------------------------------------------------------
  function casNextReq1(ks,ctl){
    var myDiagram=generateGraph(ks);
    var statesMarked = [];
    // on rajoute l'état qui vérifie req1 et l'état précédent celui-ci à statesMarked
    for (var i = 0; i < ks.etatsTransitions.length; i++){
      if(ks.etatsTransitions[i][1]===ctl){
        statesMarked.push(ks.etatsTransitions[i][2]);
        statesMarked.push(ks.etatsTransitions[i][0]);
        changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"#00FF00");
        changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"#00FF00");
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
        changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
        changeNodeColor(myDiagram,ks.etatsTransitions[i][0],"#00FF00");
        changeSubNodeColor(myDiagram,ks.etatsTransitions[i][0],"#00FF00");
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][0],"#00FF00",2);
        changeNodeBord(myDiagram,ks.etatsTransitions[i][0],"red",2);
      }
      else if(getNode(myDiagram,ks.etatsTransitions[i][2]).couleur!="#00FF00"){
        changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
        changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
      }
    }
    // Enfin on enlève les doublons
    statesMarked = retireDoublons(statesMarked);
    return statesMarked;
  }
//..............................................................................


//------------------------------------------------------------------------------
//------------------- Fonction pour vérifier Ereq1Ucs1 -------------------------
//------------------------------------------------------------------------------
  function casEU(ks, req1, cs1){
    var myDiagram=generateGraph(ks);
    statesMarked = [];
    var mark1 = [];
    var mark2 = [];
    //on effectue le marquage de req1 et cs1
    for (var i = 0; i < ks.etatsTransitions.length; i++){
      if(ks.etatsTransitions[i][1] === req1){
        mark1.push(ks.etatsTransitions[i][2]);
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
        changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
        changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
      }
      else if (ks.etatsTransitions[i][1] === cs1){
        mark2.push(ks.etatsTransitions[i][2]);
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
        changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
        changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
      }
      else if(getNode(myDiagram,ks.etatsTransitions[i][2]).bord!="#00FF00"){
        changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
        changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
      }
    }
    mark1 = retireDoublons(mark1);
    mark2 = retireDoublons(mark2);

    //on définie seenBefore et L
    var seenBefore = [];
    var L = mark2.slice();

    while(L.length != 0) {
      var m = L[0];
      statesMarked.push(m);
      changeNodeColor(myDiagram,m,"#00FF00");
      changeSubNodeColor(myDiagram,m,"#00FF00");
      //shift supprime le premier élément de L
      L.shift();
      //on cherche n, les états précèdent l'état m
      for (var i = 0; i < ks.etatsTransitions.length; i++){
        if (ks.etatsTransitions[i][2] === m){
          var n = ks.etatsTransitions[i][0];
          //si n n'est pas dans seenBefore, on l'ajoute
          if (seenBefore.indexOf(n) == -1){
            seenBefore.push(n);
            //si n est dans mark1, on l'ajoute à L
            if (mark1.indexOf(n)!=-1){
              L.unshift(n);
              changeNodeBord(myDiagram,n,"red",2);
              changeSubNodeBord(myDiagram,n,"red",2);
            }
          }
        }
      }
    }
    return statesMarked;
  }
//..............................................................................


//------------------------------------------------------------------------------
//------------------- Fonction pour vérifier Areq1Ucs1 -------------------------
//------------------------------------------------------------------------------
    function casAU(ks, req1, cs1){
      var myDiagram=generateGraph(ks);
      statesMarked = [];
      var mark1 = [];
      var mark2 = [];
      //on effectue le marquage de req1 et cs1
      for (var i = 0; i < ks.etatsTransitions.length; i++){
        if(ks.etatsTransitions[i][1] === req1){
          mark1.push(ks.etatsTransitions[i][2]);
          changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
          changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
          changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
          changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        }
        else if (ks.etatsTransitions[i][1] === cs1){
          mark2.push(ks.etatsTransitions[i][2]);
          changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"#00FF00",2);
          changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
          changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
          changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
        }
        else if(getNode(myDiagram,ks.etatsTransitions[i][2]).bord!="#00FF00"){
          changeNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
          changeNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
          changeSubNodeColor(myDiagram,ks.etatsTransitions[i][2],"red");
          changeSubNodeBord(myDiagram,ks.etatsTransitions[i][2],"red",2);
        }
      }
      mark1 = retireDoublons(mark1);
      mark2 = retireDoublons(mark2);
      var L = [];
      var nb = [];
      //On initialise nb à 0
      for (var i = 0; i < ks.etats.length; i++){
        nb[i] = 0;
      }
      //on ajoute le degré de chaque état
      for (var i = 0; i < ks.etatsTransitions.length; i++){
        nb[ks.etatsTransitions[i][0]]++;
      }
      //On ajoute à L le marquage de cs1
      L = mark2.slice();

      while(L.length != 0) {
        var m = L[0];
        statesMarked.push(m);
        changeNodeColor(myDiagram,m,"#00FF00");
        changeSubNodeColor(myDiagram,m,"#00FF00");
        //shift supprime le premier élément de L
        L.shift();
        for (var i = 0; i < ks.etatsTransitions.length; i++){
          if (ks.etatsTransitions[i][2] === m){
            if (ks.etatsTransitions[i][0] != 0) { nb[ks.etatsTransitions[i][0]]--; } // On retire 1 au degré de cet état
          }
          if(nb[ks.etatsTransitions[i][0]] === 0 && mark1.indexOf(ks.etatsTransitions[i][0]) != -1 && statesMarked.indexOf(ks.etatsTransitions[i][0]) === -1){
            L.unshift(ks.etatsTransitions[i][0]); // On ajoute cet état à L
            changeNodeBord(myDiagram,ks.etatsTransitions[i][0],"red",2);
            changeSubNodeBord(myDiagram,ks.etatsTransitions[i][0],"red",2);
          }
        }
      }
      statesMarked = retireDoublons(statesMarked);
      return statesMarked;
    }
//..............................................................................


//==============================================================================
//======================= Fonctions Subsidiaires ===============================
//==============================================================================


//------------------------------------------------------------------------------
//--------- Fonction pour afficher le résultat de la vérification --------------
//------------------------------------------------------------------------------
function afficheResultat(resultat){
  //Si le tableau n'est pas vide, la formule est vérifiée
  //on cree le message et on l'insère dans la balise p crée en amont
  if(resultat.length){
    var resultat="La formule CTL est vérifiée par la structure de Kripke. Les états de la structure de Kripke qui la vérifient sont : "+resultat+".";
      $('#resultat').html(resultat);
  }
  else{
    $('#resultat').html("Aucun état de la structure de Kripke ne vérifie la formule CTL.");
  }
}
//..............................................................................


//------------------------------------------------------------------------------
//--------- Fonction pour retirer les doublons d'un tableau --------------------
//------------------------------------------------------------------------------
function retireDoublons(tableau){
  tableau = Array.from(new Set(tableau)); //On enlève les doublons
  return tableau;
}
//..............................................................................


//------------------------------------------------------------------------------
//Fonction qui renvoie un tableau contenant toutes les propositions atomiques présente dans une structure de Kripke
//------------------------------------------------------------------------------
function findPA(ks){
  var pa=[];
  for(var i=0; i<ks.etatsTransitions.length; i++){
    pa.push(ks.etatsTransitions[i][1]);
  }
  pa = retireDoublons(pa);
  return pa;
}
//..............................................................................



//==============================================================================
//============ Fonctions pour afficher la structure de Kripke ==================
//==============================================================================


//------------------------------------------------------------------------------
//Fonction qui génère une représentation graphique de ks et retourne le diagram crée
//------------------------------------------------------------------------------
function generateGraph(ks){
  //on supprime et on recréer une nouvelle div à chaque fois pour refaire un graph
  var childDiv = "<div id='null' class='mt-4 col-12' style='width:900px; height:600px; background-color: #DAE4E4;'></div>";
  var containerChild = $('#diagramContainer').children(0);
  if(containerChild.attr('id')=="myDiagramDiv"){
    containerChild.remove();
    $('#diagramContainer').append(childDiv);
    $('#diagramContainer').children(0).attr('id',"myDiagramDiv1");
  }
  else{
    containerChild.remove();
    $('#diagramContainer').append(childDiv);
    $('#diagramContainer').children(0).attr('id',"myDiagramDiv");
  }
  var GO = go.GraphObject.make;
  var myDiagram =
  GO(go.Diagram, $('#diagramContainer').children(0).attr('id'),
  { // undoManager.isEnabled mis a false pour éviter que l'utilisateur ne modifie le graph
    initialAutoScale: go.Diagram.UniformToFill,
            layout: GO(go.LayeredDigraphLayout),
    "undoManager.isEnabled": false,
  });
  //Template des nodes
  myDiagram.nodeTemplate =
      GO(go.Node, "Auto",
        GO(go.Shape, "Ellipse",
          { width: 60, height: 60, strokeWidth:1},
          new go.Binding("fill", "color"),
          new go.Binding("stroke", "outline"),
          new go.Binding("strokeWidth", "epaisseur")
        ),
        GO(go.Shape, "Ellipse",
          { width: 40, height: 40, strokeWidth:1},
          new go.Binding("fill", "couleur"),
          new go.Binding("stroke", "bord"),
          new go.Binding("strokeWidth", "SNepaisseur")
        ),
        GO(go.TextBlock,
          new go.Binding("text", "key"))
      );

  //Template des liens
  myDiagram.linkTemplate =
      GO(go.Link,
        GO(go.Shape),
        GO(go.Shape, { toArrow: "Standard" }),
        GO(go.TextBlock, { segmentOffset: new go.Point(0, -10) },
        new go.Binding("text", "pa"))
      );
  //nodeDataArray et linkDataArray vont contenir les informations des nodes et des liens qui représentent ks
  var nodeDataArray = [];
  var linkDataArray = [];
  //on créer un node invisible qui sera lié à l'état initial afin le représenter
  var initNode = {
    key: "",
    color:"#11ffee00",
    outline:"#11ffee00",
    couleur:"#11ffee00",
    bord:"#11ffee00"
  };
  nodeDataArray.push(initNode);
  //on remplis nodeDataArray avec les nodes de ks
  for (var i = 0; i<ks.etats.length; i++){
    var nodeData = {
      key : ks.etats[i],
      color:"white",
      outline:"black",
      couleur:"white",
      bord:"white"
    };
    nodeDataArray.push(nodeData);
  }
  var initLink = {
    from: "",
    to: ks.etatInit,
  }
  linkDataArray.push(initLink);
  //on remplis linkDataArray avec les liens de ks
  for (var j = 0; j<ks.etatsTransitions.length; j++){
    var linkData = {
      from: ks.etatsTransitions[j][0],
      to: ks.etatsTransitions[j][2],
      pa: ks.etatsTransitions[j][1]
    };
    linkDataArray.push(linkData);
  }
  //on dessine le graphique
  myDiagram.model = new go.GraphLinksModel(nodeDataArray, linkDataArray);
  return myDiagram;
}
//..............................................................................


//------------------------------------------------------------------------------
//------------------ Fonction qui return la node recherché ---------------------
//------------------------------------------------------------------------------
function getNode(myDiagram,cle){
  //on cherche la node avec findNodeForKey
  var node = myDiagram.findNodeForKey(cle);
  return node.data;
}
//..............................................................................


//------------------------------------------------------------------------------
//--------------- Fonction qui change la couleur d'une node --------------------
//------------------------------------------------------------------------------
function changeNodeColor(myDiagram,cle,color){
  //node contient la node à modifier
  var node = myDiagram.findNodeForKey(cle);
  // on modifie la couleur de la node avec model.commit
  myDiagram.model.commit(function(m) {

    m.set(node.data, "color", color);

  }, "change node color");
}
//..............................................................................


//------------------------------------------------------------------------------
//---- Fonction qui change la couleur et l'épaisseur des bords d'une node ------
//------------------------------------------------------------------------------
function changeNodeBord(myDiagram,cle,color,epaisseur){
  var node = myDiagram.findNodeForKey(cle);

  myDiagram.model.commit(function(m) {

    m.set(node.data, "outline", color);
    m.set(node.data, "epaisseur", epaisseur);

  }, "change node color and node strokeWidth");
}
//..............................................................................


//------------------------------------------------------------------------------
//------------ Fonction qui change la couleur d'une sous node ------------------
//------------------------------------------------------------------------------
function changeSubNodeColor(myDiagram,cle,color){
  var node = myDiagram.findNodeForKey(cle);

  myDiagram.model.commit(function(m) {

    m.set(node.data, "couleur", color);

  }, "change subnode color");
}
//..............................................................................


//------------------------------------------------------------------------------
//-------- Fonction qui change la couleur des bords d'une sous node ------------
//------------------------------------------------------------------------------
function changeSubNodeBord(myDiagram,cle,color,SNepaisseur){
  var node = myDiagram.findNodeForKey(cle);
  myDiagram.model.commit(function(m) {

    m.set(node.data, "bord", color);
    m.set(node.data, "SNepaisseur", SNepaisseur);

  }, "change subnode border color ");
}
//..............................................................................
